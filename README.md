[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffvtt-brasil%2Fcod%2F-%2Fraw%2Fmain%2Fcod_pt-BR%2Fmodule.json)](https://foundryvtt.com/packages/translation_cod_ptBR) [![Min Core](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Min%20Core&prefix=v&query=minimumCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffvtt-brasil%2Fcod%2F-%2Fraw%2Fmain%2Fcod_pt-BR%2Fmodule.json)](https://gitlab.com/fvtt-brasil/cod) [![Compatible](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffvtt-brasil%2Fcod%2F-%2Fraw%2Fmain%2Fcod_pt-BR%2Fmodule.json)](https://gitlab.com/fvtt-brasil/cod) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&logoColor=white)](https://discord.gg/Ay42VG)

Foundry VTT CoD 2E Brazilian Portuguese
===========================================


## Português ([english version](https://gitlab.com/fvtt-brasil/cod/-/edit/main/README.md#english "English version"))

Esse módulo adiciona o idioma *Português (Brasil)* como uma opção a ser selecionada nas configurações do Foundry VTT. Selecionar essa opção traduzirá vários aspectos da aplicação relacionados ao sistema [Chronicles of Darkness 2e](https://gitlab.com/MarlQ/mta "Chronicles of Darkness 2e").
  
Esse módulo traduz somente aspectos relacionados ao sistema de Chronicles of Darkness 2e. Esse módulo não traduz outras partes do software Foundry VTT, como a interface principal. Para isso, confira o módulo [Brazilian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Se você quer ajudar nesse projeto, seja com contribuições para o código ou tradução, por favor leia o nosso [guia de contribuições](https://gitlab.com/fvtt-brasil/cod/-/blob/main/CONTRIBUTING.md), toda ajuda é bem vinda.

### Instalação

A tradução está disponível na lista de Módulos Complementares para instalar com o nome de `Português (Brasil) [CoD 2e]`.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/fvtt-brasil/cod/-/raw/main/cod_pt-BR/module.json`

#### Instalação Manual

Se as opções acima não funcionarem, visite a página com os [pacotes](https://gitlab.com/fvtt-brasil/cod/-/packages), clique na versão que você deseja usar, faça o download do arquivo `cod_pt-BR.zip` e extraia o conteúdo na pasta `%LocalAppData%/FoundryVTT/Data/modules` no Windows ou `$HOME/.local/share/FoundryVTT/Data/modules` no Linux.

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.

<br/>

## English

This module adds the language *Portuguese (Brazil)* as an option to be defined in the Foundry VTT settings. Selecting this option will translate several aspects of the application related to the [Chronicles of Darkness 2e](https://gitlab.com/MarlQ/mta "Chronicles of Darkness 2e") system.
  
This module only translates aspects related to the Chronicles of Darkness 2e system. This module does not translate other parts of the Foundry VTT software, such as the main interface. For this, check out the module [Brazilian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

If you want to help with this project, either with contributions to the code or translation, please read our [contribution guide](https://gitlab.com/fvtt-brasil/cod/-/blob/main/CONTRIBUTING.md), all help is welcome.

### Installation

The translation is available in the list of Complementary Modules to install with the name of `Português (Brasil) [CoD 2e]`.

#### Installation by Manifest

In the `Add-On Modules` option, click on `Install Module` and place the following link in the `Manifest URL` field

`https://gitlab.com/fvtt-brasil/cod/-/raw/main/cod_pt-BR/module.json`

#### Manual Installation

If the above options do not work, visit the page with the [packages](https://gitlab.com/fvtt-brasil/cod/-/packages), click on the version you want to use, download the file `cod_pt-BR.zip` and extract its contents into the `%LocalAppData%/FoundryVTT/Data/modules` folder on Windows or `$HOME/.local/share/FoundryVTT/Data/modules` on Linux.

Once this is done, activate the module in the settings of the world in which you want it and then change the language in the settings.
